# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.apis.path_to_api import path_to_api

import enum


class PathValues(str, enum.Enum):
    V1ALPHA1_TENANTS_TENANT_JWKS_JWKCREATE_SESSION = "/v1alpha1/tenants/{tenant}/jwks/{jwk}:createSession"
    V1ALPHA1_TENANTS_TENANT_JWKS_JWKREFRESH_SESSION = "/v1alpha1/tenants/{tenant}/jwks/{jwk}:refreshSession"
