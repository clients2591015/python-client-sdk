from openapi_client.paths.v1alpha1_tenants_tenant_jwks_jwkrefresh_session.post import ApiForpost


class V1alpha1TenantsTenantJwksJwkrefreshSession(
    ApiForpost,
):
    pass
