from openapi_client.paths.v1alpha1_tenants_tenant_jwks_jwkcreate_session.post import ApiForpost


class V1alpha1TenantsTenantJwksJwkcreateSession(
    ApiForpost,
):
    pass
