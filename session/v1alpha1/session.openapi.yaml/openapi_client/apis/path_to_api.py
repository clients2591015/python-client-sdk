import typing_extensions

from openapi_client.paths import PathValues
from openapi_client.apis.paths.v1alpha1_tenants_tenant_jwks_jwkcreate_session import V1alpha1TenantsTenantJwksJwkcreateSession
from openapi_client.apis.paths.v1alpha1_tenants_tenant_jwks_jwkrefresh_session import V1alpha1TenantsTenantJwksJwkrefreshSession

PathToApi = typing_extensions.TypedDict(
    'PathToApi',
    {
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS_JWKCREATE_SESSION: V1alpha1TenantsTenantJwksJwkcreateSession,
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS_JWKREFRESH_SESSION: V1alpha1TenantsTenantJwksJwkrefreshSession,
    }
)

path_to_api = PathToApi(
    {
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS_JWKCREATE_SESSION: V1alpha1TenantsTenantJwksJwkcreateSession,
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS_JWKREFRESH_SESSION: V1alpha1TenantsTenantJwksJwkrefreshSession,
    }
)
