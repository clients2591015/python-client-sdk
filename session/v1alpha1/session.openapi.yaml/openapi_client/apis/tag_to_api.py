import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.session_manager_api import SessionManagerApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.SESSION_MANAGER: SessionManagerApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.SESSION_MANAGER: SessionManagerApi,
    }
)
