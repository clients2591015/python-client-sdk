<a name="__pageTop"></a>
# openapi_client.apis.tags.session_manager_api.SessionManagerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**session_manager_create_session**](#session_manager_create_session) | **post** /v1alpha1/tenants/{tenant}/jwks/{jwk}:createSession | 
[**session_manager_refresh_session**](#session_manager_refresh_session) | **post** /v1alpha1/tenants/{tenant}/jwks/{jwk}:refreshSession | 

# **session_manager_create_session**
<a name="session_manager_create_session"></a>
> OrgMequSessionV1alpha1Session session_manager_create_session(tenantjwkorg_mequ_session_v1alpha1_create_session_request)



### Example

```python
import openapi_client
from openapi_client.apis.tags import session_manager_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_session_v1alpha1_session import OrgMequSessionV1alpha1Session
from openapi_client.model.org_mequ_session_v1alpha1_create_session_request import OrgMequSessionV1alpha1CreateSessionRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = session_manager_api.SessionManagerApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
        'jwk': "jwk_example",
    }
    body = OrgMequSessionV1alpha1CreateSessionRequest(
        parent="parent_example",
        user_id="user_id_example",
        device=OrgMequSessionV1alpha1Device(
            device_id="device_id_example",
            device_name="device_name_example",
            device_type="device_type_example",
        ),
        session=OrgMequSessionV1alpha1Session(
            name="name_example",
            token="token_example",
            refresh="refresh_example",
        ),
    )
    try:
        api_response = api_instance.session_manager_create_session(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SessionManagerApi->session_manager_create_session: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha1CreateSessionRequest**](../../models/OrgMequSessionV1alpha1CreateSessionRequest.md) |  | 


### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 
jwk | JwkSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# JwkSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#session_manager_create_session.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#session_manager_create_session.ApiResponseForDefault) | Default error response

#### session_manager_create_session.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha1Session**](../../models/OrgMequSessionV1alpha1Session.md) |  | 


#### session_manager_create_session.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **session_manager_refresh_session**
<a name="session_manager_refresh_session"></a>
> OrgMequSessionV1alpha1Session session_manager_refresh_session(tenantjwkorg_mequ_session_v1alpha1_refresh_session_request)



refresh session

### Example

```python
import openapi_client
from openapi_client.apis.tags import session_manager_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_session_v1alpha1_session import OrgMequSessionV1alpha1Session
from openapi_client.model.org_mequ_session_v1alpha1_refresh_session_request import OrgMequSessionV1alpha1RefreshSessionRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = session_manager_api.SessionManagerApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
        'jwk': "jwk_example",
    }
    body = OrgMequSessionV1alpha1RefreshSessionRequest(
        parent="parent_example",
        refresh="refresh_example",
    )
    try:
        api_response = api_instance.session_manager_refresh_session(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SessionManagerApi->session_manager_refresh_session: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha1RefreshSessionRequest**](../../models/OrgMequSessionV1alpha1RefreshSessionRequest.md) |  | 


### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 
jwk | JwkSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# JwkSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#session_manager_refresh_session.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#session_manager_refresh_session.ApiResponseForDefault) | Default error response

#### session_manager_refresh_session.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha1Session**](../../models/OrgMequSessionV1alpha1Session.md) |  | 


#### session_manager_refresh_session.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

