# openapi_client.model.org_mequ_session_v1alpha1_create_session_request.OrgMequSessionV1alpha1CreateSessionRequest

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**parent** | str,  | str,  |  | 
**userID** | str,  | str,  |  | [optional] 
**device** | [**OrgMequSessionV1alpha1Device**](OrgMequSessionV1alpha1Device.md) | [**OrgMequSessionV1alpha1Device**](OrgMequSessionV1alpha1Device.md) |  | [optional] 
**session** | [**OrgMequSessionV1alpha1Session**](OrgMequSessionV1alpha1Session.md) | [**OrgMequSessionV1alpha1Session**](OrgMequSessionV1alpha1Session.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

