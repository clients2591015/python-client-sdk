<a name="__pageTop"></a>
# openapi_client.apis.tags.session_service_api.SessionServiceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**session_service_new_session**](#session_service_new_session) | **post** /v1alpha2/session | 
[**session_service_refresh_session**](#session_service_refresh_session) | **post** /v1alpha2/session/refresh | 

# **session_service_new_session**
<a name="session_service_new_session"></a>
> OrgMequSessionV1alpha2Session session_service_new_session(org_mequ_session_v1alpha2_session_request)



new session creates a new session for a user.

### Example

```python
import openapi_client
from openapi_client.apis.tags import session_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_session_v1alpha2_session import OrgMequSessionV1alpha2Session
from openapi_client.model.org_mequ_session_v1alpha2_session_request import OrgMequSessionV1alpha2SessionRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = session_service_api.SessionServiceApi(api_client)

    # example passing only required values which don't have defaults set
    body = OrgMequSessionV1alpha2SessionRequest(
        user_id="user_id_example",
        device_id="device_id_example",
    )
    try:
        api_response = api_instance.session_service_new_session(
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SessionServiceApi->session_service_new_session: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha2SessionRequest**](../../models/OrgMequSessionV1alpha2SessionRequest.md) |  | 


### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#session_service_new_session.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#session_service_new_session.ApiResponseForDefault) | Default error response

#### session_service_new_session.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha2Session**](../../models/OrgMequSessionV1alpha2Session.md) |  | 


#### session_service_new_session.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **session_service_refresh_session**
<a name="session_service_refresh_session"></a>
> OrgMequSessionV1alpha2Session session_service_refresh_session(org_mequ_session_v1alpha2_session)



refresh session refreshes a session for a user.

### Example

```python
import openapi_client
from openapi_client.apis.tags import session_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_session_v1alpha2_session import OrgMequSessionV1alpha2Session
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = session_service_api.SessionServiceApi(api_client)

    # example passing only required values which don't have defaults set
    body = OrgMequSessionV1alpha2Session(
        name="name_example",
        token="token_example",
        refresh_token="refresh_token_example",
    )
    try:
        api_response = api_instance.session_service_refresh_session(
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SessionServiceApi->session_service_refresh_session: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha2Session**](../../models/OrgMequSessionV1alpha2Session.md) |  | 


### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#session_service_refresh_session.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#session_service_refresh_session.ApiResponseForDefault) | Default error response

#### session_service_refresh_session.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequSessionV1alpha2Session**](../../models/OrgMequSessionV1alpha2Session.md) |  | 


#### session_service_refresh_session.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

