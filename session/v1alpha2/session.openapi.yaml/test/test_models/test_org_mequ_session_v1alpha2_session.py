# coding: utf-8

"""
    SessionService API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""

import unittest

import openapi_client
from openapi_client.model.org_mequ_session_v1alpha2_session import OrgMequSessionV1alpha2Session
from openapi_client import configuration


class TestOrgMequSessionV1alpha2Session(unittest.TestCase):
    """OrgMequSessionV1alpha2Session unit test stubs"""
    _configuration = configuration.Configuration()


if __name__ == '__main__':
    unittest.main()
