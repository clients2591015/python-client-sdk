import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.session_service_api import SessionServiceApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.SESSION_SERVICE: SessionServiceApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.SESSION_SERVICE: SessionServiceApi,
    }
)
