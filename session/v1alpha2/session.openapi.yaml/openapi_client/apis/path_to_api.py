import typing_extensions

from openapi_client.paths import PathValues
from openapi_client.apis.paths.v1alpha2_session import V1alpha2Session
from openapi_client.apis.paths.v1alpha2_session_refresh import V1alpha2SessionRefresh

PathToApi = typing_extensions.TypedDict(
    'PathToApi',
    {
        PathValues.V1ALPHA2_SESSION: V1alpha2Session,
        PathValues.V1ALPHA2_SESSION_REFRESH: V1alpha2SessionRefresh,
    }
)

path_to_api = PathToApi(
    {
        PathValues.V1ALPHA2_SESSION: V1alpha2Session,
        PathValues.V1ALPHA2_SESSION_REFRESH: V1alpha2SessionRefresh,
    }
)
