from openapi_client.paths.v1alpha2_session_refresh.post import ApiForpost


class V1alpha2SessionRefresh(
    ApiForpost,
):
    pass
