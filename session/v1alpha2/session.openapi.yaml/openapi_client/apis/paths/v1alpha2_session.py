from openapi_client.paths.v1alpha2_session.post import ApiForpost


class V1alpha2Session(
    ApiForpost,
):
    pass
