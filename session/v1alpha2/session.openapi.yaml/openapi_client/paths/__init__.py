# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.apis.path_to_api import path_to_api

import enum


class PathValues(str, enum.Enum):
    V1ALPHA2_SESSION = "/v1alpha2/session"
    V1ALPHA2_SESSION_REFRESH = "/v1alpha2/session/refresh"
