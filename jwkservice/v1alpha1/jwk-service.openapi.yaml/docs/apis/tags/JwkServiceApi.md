<a name="__pageTop"></a>
# openapi_client.apis.tags.jwk_service_api.JwkServiceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jwk_service_create_jwk_set**](#jwk_service_create_jwk_set) | **post** /v1alpha1/tenants/{tenant}/jwks | 
[**jwk_service_get_jwk_set_pub_key**](#jwk_service_get_jwk_set_pub_key) | **get** /v1alpha1/.wellknown/tenants/{tenant}/jwks/{jwk} | 

# **jwk_service_create_jwk_set**
<a name="jwk_service_create_jwk_set"></a>
> OrgMequJwkServiceV1JwKConfig jwk_service_create_jwk_set(tenantorg_mequ_jwk_service_v1_create_jwk_set_request)



### Example

```python
import openapi_client
from openapi_client.apis.tags import jwk_service_api
from openapi_client.model.org_mequ_jwk_service_v1_jw_k_config import OrgMequJwkServiceV1JwKConfig
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_jwk_service_v1_create_jwk_set_request import OrgMequJwkServiceV1CreateJwkSetRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jwk_service_api.JwkServiceApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
    }
    body = OrgMequJwkServiceV1CreateJwkSetRequest(
        parent="parent_example",
        jwk_config=OrgMequJwkServiceV1JwKConfig(
            name="name_example",
            title="title_example",
            description="description_example",
            count=1,
        ),
    )
    try:
        api_response = api_instance.jwk_service_create_jwk_set(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling JwkServiceApi->jwk_service_create_jwk_set: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequJwkServiceV1CreateJwkSetRequest**](../../models/OrgMequJwkServiceV1CreateJwkSetRequest.md) |  | 


### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#jwk_service_create_jwk_set.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#jwk_service_create_jwk_set.ApiResponseForDefault) | Default error response

#### jwk_service_create_jwk_set.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequJwkServiceV1JwKConfig**](../../models/OrgMequJwkServiceV1JwKConfig.md) |  | 


#### jwk_service_create_jwk_set.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **jwk_service_get_jwk_set_pub_key**
<a name="jwk_service_get_jwk_set_pub_key"></a>
> OrgMequJwkServiceV1JwkSetPublicKeys jwk_service_get_jwk_set_pub_key(tenantjwk)



### Example

```python
import openapi_client
from openapi_client.apis.tags import jwk_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_jwk_service_v1_jwk_set_public_keys import OrgMequJwkServiceV1JwkSetPublicKeys
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jwk_service_api.JwkServiceApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
        'jwk': "jwk_example",
    }
    try:
        api_response = api_instance.jwk_service_get_jwk_set_pub_key(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling JwkServiceApi->jwk_service_get_jwk_set_pub_key: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 
jwk | JwkSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# JwkSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#jwk_service_get_jwk_set_pub_key.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#jwk_service_get_jwk_set_pub_key.ApiResponseForDefault) | Default error response

#### jwk_service_get_jwk_set_pub_key.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequJwkServiceV1JwkSetPublicKeys**](../../models/OrgMequJwkServiceV1JwkSetPublicKeys.md) |  | 


#### jwk_service_get_jwk_set_pub_key.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

