# openapi_client.model.org_mequ_jwk_service_v1_create_jwk_set_request.OrgMequJwkServiceV1CreateJwkSetRequest

JwkService is the service to manage JWKs

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | JwkService is the service to manage JWKs | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**parent** | str,  | str,  |  | [optional] 
**jwkConfig** | [**OrgMequJwkServiceV1JwKConfig**](OrgMequJwkServiceV1JwKConfig.md) | [**OrgMequJwkServiceV1JwKConfig**](OrgMequJwkServiceV1JwKConfig.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

