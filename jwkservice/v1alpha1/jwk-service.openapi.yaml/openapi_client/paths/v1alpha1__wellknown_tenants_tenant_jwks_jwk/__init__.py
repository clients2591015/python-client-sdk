# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1alpha1__wellknown_tenants_tenant_jwks_jwk import Api

from openapi_client.paths import PathValues

path = PathValues.V1ALPHA1__WELLKNOWN_TENANTS_TENANT_JWKS_JWK