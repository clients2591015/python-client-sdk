import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.jwk_service_api import JwkServiceApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.JWK_SERVICE: JwkServiceApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.JWK_SERVICE: JwkServiceApi,
    }
)
