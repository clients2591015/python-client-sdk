import typing_extensions

from openapi_client.paths import PathValues
from openapi_client.apis.paths.v1alpha1__wellknown_tenants_tenant_jwks_jwk import V1alpha1WellknownTenantsTenantJwksJwk
from openapi_client.apis.paths.v1alpha1_tenants_tenant_jwks import V1alpha1TenantsTenantJwks

PathToApi = typing_extensions.TypedDict(
    'PathToApi',
    {
        PathValues.V1ALPHA1__WELLKNOWN_TENANTS_TENANT_JWKS_JWK: V1alpha1WellknownTenantsTenantJwksJwk,
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS: V1alpha1TenantsTenantJwks,
    }
)

path_to_api = PathToApi(
    {
        PathValues.V1ALPHA1__WELLKNOWN_TENANTS_TENANT_JWKS_JWK: V1alpha1WellknownTenantsTenantJwksJwk,
        PathValues.V1ALPHA1_TENANTS_TENANT_JWKS: V1alpha1TenantsTenantJwks,
    }
)
