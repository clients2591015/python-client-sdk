from openapi_client.paths.v1alpha1__wellknown_tenants_tenant_jwks_jwk.get import ApiForget


class V1alpha1WellknownTenantsTenantJwksJwk(
    ApiForget,
):
    pass
