from openapi_client.paths.v1alpha1_tenants_tenant_jwks.post import ApiForpost


class V1alpha1TenantsTenantJwks(
    ApiForpost,
):
    pass
