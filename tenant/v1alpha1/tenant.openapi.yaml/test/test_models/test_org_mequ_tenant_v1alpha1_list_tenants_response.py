# coding: utf-8

"""
    TenantService API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""

import unittest

import openapi_client
from openapi_client.model.org_mequ_tenant_v1alpha1_list_tenants_response import OrgMequTenantV1alpha1ListTenantsResponse
from openapi_client import configuration


class TestOrgMequTenantV1alpha1ListTenantsResponse(unittest.TestCase):
    """OrgMequTenantV1alpha1ListTenantsResponse unit test stubs"""
    _configuration = configuration.Configuration()


if __name__ == '__main__':
    unittest.main()
