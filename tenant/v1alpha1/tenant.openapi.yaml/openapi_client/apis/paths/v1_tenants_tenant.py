from openapi_client.paths.v1_tenants_tenant.get import ApiForget
from openapi_client.paths.v1_tenants_tenant.delete import ApiFordelete


class V1TenantsTenant(
    ApiForget,
    ApiFordelete,
):
    pass
