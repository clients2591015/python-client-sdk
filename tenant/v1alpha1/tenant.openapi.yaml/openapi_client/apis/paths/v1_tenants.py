from openapi_client.paths.v1_tenants.get import ApiForget
from openapi_client.paths.v1_tenants.post import ApiForpost


class V1Tenants(
    ApiForget,
    ApiForpost,
):
    pass
