import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.tenant_service_api import TenantServiceApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.TENANT_SERVICE: TenantServiceApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.TENANT_SERVICE: TenantServiceApi,
    }
)
