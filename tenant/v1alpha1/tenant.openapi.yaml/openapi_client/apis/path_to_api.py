import typing_extensions

from openapi_client.paths import PathValues
from openapi_client.apis.paths.v1_tenants import V1Tenants
from openapi_client.apis.paths.v1_tenants_tenant import V1TenantsTenant

PathToApi = typing_extensions.TypedDict(
    'PathToApi',
    {
        PathValues.V1_TENANTS: V1Tenants,
        PathValues.V1_TENANTS_TENANT: V1TenantsTenant,
    }
)

path_to_api = PathToApi(
    {
        PathValues.V1_TENANTS: V1Tenants,
        PathValues.V1_TENANTS_TENANT: V1TenantsTenant,
    }
)
