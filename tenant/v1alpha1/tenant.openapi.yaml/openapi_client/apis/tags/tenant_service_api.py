# coding: utf-8

"""
    TenantService API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_tenants.post import TenantServiceCreateTenant
from openapi_client.paths.v1_tenants_tenant.delete import TenantServiceDeleteTenant
from openapi_client.paths.v1_tenants_tenant.get import TenantServiceGetTenant
from openapi_client.paths.v1_tenants.get import TenantServiceListTenants


class TenantServiceApi(
    TenantServiceCreateTenant,
    TenantServiceDeleteTenant,
    TenantServiceGetTenant,
    TenantServiceListTenants,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
