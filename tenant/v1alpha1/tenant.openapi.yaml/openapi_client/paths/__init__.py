# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.apis.path_to_api import path_to_api

import enum


class PathValues(str, enum.Enum):
    V1_TENANTS = "/v1/tenants"
    V1_TENANTS_TENANT = "/v1/tenants/{tenant}"
