<a name="__pageTop"></a>
# openapi_client.apis.tags.tenant_service_api.TenantServiceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tenant_service_create_tenant**](#tenant_service_create_tenant) | **post** /v1/tenants | 
[**tenant_service_delete_tenant**](#tenant_service_delete_tenant) | **delete** /v1/tenants/{tenant} | 
[**tenant_service_get_tenant**](#tenant_service_get_tenant) | **get** /v1/tenants/{tenant} | 
[**tenant_service_list_tenants**](#tenant_service_list_tenants) | **get** /v1/tenants | 

# **tenant_service_create_tenant**
<a name="tenant_service_create_tenant"></a>
> OrgMequTenantV1alpha1Tenant tenant_service_create_tenant(org_mequ_tenant_v1alpha1_tenant)



### Example

```python
import openapi_client
from openapi_client.apis.tags import tenant_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_tenant_v1alpha1_tenant import OrgMequTenantV1alpha1Tenant
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenant_service_api.TenantServiceApi(api_client)

    # example passing only required values which don't have defaults set
    body = OrgMequTenantV1alpha1Tenant(
        name="name_example",
        title="title_example",
        description="description_example",
        api_key="api_key_example",
        created_at="1970-01-01T00:00:00.00Z",
        updated_at="1970-01-01T00:00:00.00Z",
        deleted_at="1970-01-01T00:00:00.00Z",
    )
    try:
        api_response = api_instance.tenant_service_create_tenant(
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling TenantServiceApi->tenant_service_create_tenant: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequTenantV1alpha1Tenant**](../../models/OrgMequTenantV1alpha1Tenant.md) |  | 


### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#tenant_service_create_tenant.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#tenant_service_create_tenant.ApiResponseForDefault) | Default error response

#### tenant_service_create_tenant.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequTenantV1alpha1Tenant**](../../models/OrgMequTenantV1alpha1Tenant.md) |  | 


#### tenant_service_create_tenant.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **tenant_service_delete_tenant**
<a name="tenant_service_delete_tenant"></a>
> tenant_service_delete_tenant(tenant)



### Example

```python
import openapi_client
from openapi_client.apis.tags import tenant_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenant_service_api.TenantServiceApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
    }
    try:
        api_response = api_instance.tenant_service_delete_tenant(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling TenantServiceApi->tenant_service_delete_tenant: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#tenant_service_delete_tenant.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#tenant_service_delete_tenant.ApiResponseForDefault) | Default error response

#### tenant_service_delete_tenant.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

#### tenant_service_delete_tenant.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **tenant_service_get_tenant**
<a name="tenant_service_get_tenant"></a>
> OrgMequTenantV1alpha1Tenant tenant_service_get_tenant(tenant)



### Example

```python
import openapi_client
from openapi_client.apis.tags import tenant_service_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_tenant_v1alpha1_tenant import OrgMequTenantV1alpha1Tenant
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenant_service_api.TenantServiceApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'tenant': "tenant_example",
    }
    try:
        api_response = api_instance.tenant_service_get_tenant(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling TenantServiceApi->tenant_service_get_tenant: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
tenant | TenantSchema | | 

# TenantSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#tenant_service_get_tenant.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#tenant_service_get_tenant.ApiResponseForDefault) | Default error response

#### tenant_service_get_tenant.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequTenantV1alpha1Tenant**](../../models/OrgMequTenantV1alpha1Tenant.md) |  | 


#### tenant_service_get_tenant.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **tenant_service_list_tenants**
<a name="tenant_service_list_tenants"></a>
> OrgMequTenantV1alpha1ListTenantsResponse tenant_service_list_tenants()



### Example

```python
import openapi_client
from openapi_client.apis.tags import tenant_service_api
from openapi_client.model.org_mequ_tenant_v1alpha1_list_tenants_response import OrgMequTenantV1alpha1ListTenantsResponse
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenant_service_api.TenantServiceApi(api_client)

    # example passing only optional values
    query_params = {
        'pageSize': 1,
        'pageToken': "pageToken_example",
    }
    try:
        api_response = api_instance.tenant_service_list_tenants(
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling TenantServiceApi->tenant_service_list_tenants: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
pageSize | PageSizeSchema | | optional
pageToken | PageTokenSchema | | optional


# PageSizeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 32 bit integer

# PageTokenSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#tenant_service_list_tenants.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#tenant_service_list_tenants.ApiResponseForDefault) | Default error response

#### tenant_service_list_tenants.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequTenantV1alpha1ListTenantsResponse**](../../models/OrgMequTenantV1alpha1ListTenantsResponse.md) |  | 


#### tenant_service_list_tenants.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

