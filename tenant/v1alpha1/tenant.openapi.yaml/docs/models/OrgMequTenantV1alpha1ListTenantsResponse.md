# openapi_client.model.org_mequ_tenant_v1alpha1_list_tenants_response.OrgMequTenantV1alpha1ListTenantsResponse

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[tenants](#tenants)** | list, tuple,  | tuple,  | tenants is the list of tenants | [optional] 
**nextPageToken** | str,  | str,  | next_page_token is the page token to use for pagination | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# tenants

tenants is the list of tenants

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | tenants is the list of tenants | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**OrgMequTenantV1alpha1Tenant**](OrgMequTenantV1alpha1Tenant.md) | [**OrgMequTenantV1alpha1Tenant**](OrgMequTenantV1alpha1Tenant.md) | [**OrgMequTenantV1alpha1Tenant**](OrgMequTenantV1alpha1Tenant.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

