# openapi_client.model.org_mequ_tenant_v1alpha1_tenant.OrgMequTenantV1alpha1Tenant

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**name** | str,  | str,  | resource name of the tenant in the format of \&quot;tenants/{tenant_id}\&quot; | [optional] 
**title** | str,  | str,  |  | [optional] 
**description** | str,  | str,  |  | [optional] 
**apiKey** | str,  | str,  |  | [optional] 
**createdAt** | str, datetime,  | str,  | created_at is the time the tenant was created | [optional] value must conform to RFC-3339 date-time
**updatedAt** | str, datetime,  | str,  | updated_at is the time the tenant was last updated | [optional] value must conform to RFC-3339 date-time
**deletedAt** | str, datetime,  | str,  | deleted_at is the time the tenant was deleted | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

