<a name="__pageTop"></a>
# openapi_client.apis.tags.jwk_api.JWKApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**j_wk_get_public_key**](#j_wk_get_public_key) | **get** /.well-known/jwks.json | 

# **j_wk_get_public_key**
<a name="j_wk_get_public_key"></a>
> OrgMequJsonwebkeyV1alpha1JWKResponse j_wk_get_public_key()



Get Public Key whit google empty request and return json with public keys in jwk format

### Example

```python
import openapi_client
from openapi_client.apis.tags import jwk_api
from openapi_client.model.google_rpc_status import GoogleRpcStatus
from openapi_client.model.org_mequ_jsonwebkey_v1alpha1_jwk_response import OrgMequJsonwebkeyV1alpha1JWKResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jwk_api.JWKApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.j_wk_get_public_key()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling JWKApi->j_wk_get_public_key: %s\n" % e)
```
### Parameters
This endpoint does not need any parameter.

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#j_wk_get_public_key.ApiResponseFor200) | OK
default | [ApiResponseForDefault](#j_wk_get_public_key.ApiResponseForDefault) | Default error response

#### j_wk_get_public_key.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OrgMequJsonwebkeyV1alpha1JWKResponse**](../../models/OrgMequJsonwebkeyV1alpha1JWKResponse.md) |  | 


#### j_wk_get_public_key.ApiResponseForDefault
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor0ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor0ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**GoogleRpcStatus**](../../models/GoogleRpcStatus.md) |  | 


### Authorization

No authorization required

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

