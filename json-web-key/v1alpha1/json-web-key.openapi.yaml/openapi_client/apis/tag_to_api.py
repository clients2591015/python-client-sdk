import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.jwk_api import JWKApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.JWK: JWKApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.JWK: JWKApi,
    }
)
