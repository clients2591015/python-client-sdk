import typing_extensions

from openapi_client.paths import PathValues
from openapi_client.apis.paths.well_known_jwks_json import WellKnownJwksJson

PathToApi = typing_extensions.TypedDict(
    'PathToApi',
    {
        PathValues._WELLKNOWN_JWKS_JSON: WellKnownJwksJson,
    }
)

path_to_api = PathToApi(
    {
        PathValues._WELLKNOWN_JWKS_JSON: WellKnownJwksJson,
    }
)
