from openapi_client.paths.well_known_jwks_json.get import ApiForget


class WellKnownJwksJson(
    ApiForget,
):
    pass
